# Research Question - Group 32

Members: Aron Samuel Georgekutty
	 Geordin Jose
	 Jijo winny
	 Niyo Mathew

Topic: Learning based NPCs in video games

RQ: Are video games with learning based NPCs more enjoyable than the ones with pre-scripted NPCs?

Context: Video games

Population: Gaming community

Intervention: Machine learning methods

Comparison: Games with learning based NPC vs Games with pre-scripted ones

Outcome: Making video games more enjoyable and hence attract a wider audience.


